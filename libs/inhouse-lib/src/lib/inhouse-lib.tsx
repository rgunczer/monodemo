import './inhouse-lib.module.scss';

/* eslint-disable-next-line */
export interface InhouseLibProps {}

export function InhouseLib(props: InhouseLibProps) {
  return (
    <div>
      <h1>Welcome to InhouseLib!</h1>
    </div>
  );
}

export default InhouseLib;
