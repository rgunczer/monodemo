import { render } from '@testing-library/react';

import InhouseLib from './inhouse-lib';

describe('InhouseLib', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<InhouseLib />);
    expect(baseElement).toBeTruthy();
  });
});
