import CircularProgress from '@mui/material/CircularProgress';
import { LinearProgress } from '@mui/material';

import Box from '@mui/material/Box';

export const Loading = () => {
  return (
    <Box sx={{ display: 'flex', alignItems: "center", justifyContent: "center", padding: '3rem' }}>
      {/* <CircularProgress /> */}
      <Box sx={{ width: '100%' }}>
        <LinearProgress />
      </Box>
    </Box>
  )
}
