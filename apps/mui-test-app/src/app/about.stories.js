import React from 'react';
import { About } from './about';

export default {
  component: About,
  title: 'About',
};

const Template = args => <About {...args} />

export const Default = Template.bind({});
Default.args = {
    name: 'About',
    bgColor: 'yellow'
};

export const Green = Template.bind({});
Green.args = {
    name: 'About',
    bgColor: 'green'
};

export const Orange = Template.bind({});
Orange.args = {
    name: 'About',
    bgColor: 'orange'
};

export const Blue = Template.bind({});
Blue.args = {
    name: 'Blue',
    bgColor: 'lightblue'
};
