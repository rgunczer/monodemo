import { Loading } from '@laborelec/inhouse-lib'

export const About = (props: { name: string, bgColor: string}) => {
  return (
    <>
      <div style={{ backgroundColor: props.bgColor, fontSize: '2rem', fontWeight: 'bolder' }}>{props.name}</div>
      <Loading />
    </>
  )
}
