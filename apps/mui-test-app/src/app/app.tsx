import styles from './app.module.scss';

import Typography from '@mui/material/Typography'

import { Loading } from '@laborelec/inhouse-lib';

import { Route, Link } from 'react-router-dom';
import { About } from './about';

export function App() {
  return (
    <div className={styles.app}>
      <Typography mb={2} variant="h1">
        Hello
      </Typography>
      <Loading />
      <About name="some name" bgColor="lightblue" />
    </div>
  );
}

export default App;
