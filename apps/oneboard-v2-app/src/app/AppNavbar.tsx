import * as React from 'react';
import { useHistory } from 'react-router-dom';

import {
  AppBar,
  Box,
  Toolbar,
  IconButton,
  Typography,
  Badge,
  MenuItem,
  Menu,
  Button
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import NotificationsIcon from '@mui/icons-material/Notifications';
import AccountCircle from '@mui/icons-material/AccountCircle';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

interface IMenuItem {
  id: string;
  text: string;
  path?: string;
}

type MenuTypes = 'account' | 'group1' | 'group2';

const MenuStore: { [key in MenuTypes]: IMenuItem[] } = {
  account: [
    {
      id: 'account-settings',
      text: 'Settings'
    },
    {
      id: 'account-sign-out',
      text: 'Sign-out'
    }
  ],
  group1: [
    {
      id: 'list',
      text: 'List',
      path:'/group1/list'
    },
    {
      id: 'create',
      text: 'Create',
      path: '/group/create'
    },
    {
      id: 'opt1',
      text: 'Option1',
      path: '/group/create'
    },
    {
      id: 'opt2',
      text: 'Option2',
      path: '/group/create'
    },
    {
      id: 'opt3',
      text: 'Option3',
      path: '/group/create'
    },
    {
      id: 'opt4',
      text: 'Option 4',
      path: '/group/create'
    }
  ],
  group2: [
    {
      id: 'list',
      text: 'List',
      path: 'group2/list'
    },
    {
      id: 'create',
      text: 'Create',
      path: '/group2/create'
    }
  ]
}


const AppNavbar = (props: { drawer: Function }) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [menuItems, setMenuItems] = React.useState<IMenuItem[]>([]);
  const history = useHistory();
  const numberOfNotifications = 3;
  const isMenuOpen = Boolean(anchorEl);


  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    const menuName = event.currentTarget.dataset.menu ?? 'default';

    setMenuItems((MenuStore[(menuName as MenuTypes)] as IMenuItem[]))

    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null)
  };

  const handleMenuAction = (m: IMenuItem) => {
    if (m.path) {
      history.push(m.path)
    }
    handleMenuClose();
  }

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      {menuItems.map(m => <MenuItem key={m.id} onClick={() => handleMenuAction(m)}>{m.text}</MenuItem>)}
    </Menu>
  );

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed">
        <Toolbar variant="dense">
          <IconButton edge="start" color="inherit" sx={{ mr: 2 }} onClick={() => props.drawer(true)}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" component="div" style={{ cursor: 'pointer' }}  onClick={() => history.push("/") }>
            &nbsp;OneBoard V2
          </Typography>

          <Box sx={{ marginLeft: '10px', display: { xs: 'none', md: 'flex' } }}>
            <Button color="inherit" data-menu="group1" onClick={handleMenuOpen}>
              Menu Group 1
              <ArrowDropDownIcon />
            </Button>

            <Button color="inherit" data-menu="group2" onClick={handleMenuOpen}>
              Menu Group 2
              <ArrowDropDownIcon />
            </Button>

          </Box>
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            <IconButton size="large" color="inherit">
              <Badge badgeContent={numberOfNotifications} color="error">
                <NotificationsIcon />
              </Badge>
            </IconButton>

            <IconButton size="large" edge="end" data-menu="account" onClick={handleMenuOpen} color="inherit">
              <AccountCircle />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderMenu}
    </Box>
  )
}

export default AppNavbar
