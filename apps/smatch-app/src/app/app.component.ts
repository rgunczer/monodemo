import { Component } from '@angular/core';

@Component({
  selector: 'laborelec-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'smatch-app';
}
